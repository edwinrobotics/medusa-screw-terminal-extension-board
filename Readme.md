# Medusa Screw Terminal Extension Board

![Medusa Screw Terminal Extension Board](https://shop.edwinrobotics.com/4146-thickbox_default/medusa-screw-terminal-extension-board.jpg "Medusa Screw Terminal Extension Board")

[Medusa Screw Terminal Extension Board](https://shop.edwinrobotics.com/modules/1237-medusa-screw-terminal-extension-board.html)


The Medusa Screw Terminal and headers Extension board enables you to interface non Medusa products, it is provided as part of the Medusa IoT starter kit for interfacing the Servo Motor and the Magnetic Door sensor. It comes in quite handy when your project involves the use of components that are currently not part of the Medusa Line.


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.